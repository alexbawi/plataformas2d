﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MyController : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    private bool attack;
    public float fuerzaSalto = 5.0f;
    [SerializeField] private GameObject graphics;
    [SerializeField] private Animator animator;
    public bool isGrounded = false;

    private float scalaActual;

    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        animator.SetFloat("velocidadX", Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);
        attack = Input.GetButton("Fire1");

        if (attack && isGrounded)
        {
            move = 0f;
            jump = false;
            animator.SetBool("attack", attack);
        }
        else
        {
            animator.SetBool("attack", false);
        }


        if (jump && isGrounded)
        {
            Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        scalaActual = graphics.transform.localScale.x;

        if (move > 0 && scalaActual < 0)
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);
        }
        else if (move < 0 && scalaActual > 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

}